import sys
import os
import shutil
from s14 import Server14
from api_master import API_master
import argparse
import hashlib

# parse args
parser = argparse.ArgumentParser ()
parser.add_argument ('--home', help = 'path to home directory; ~ by default', default = '~')
parser.add_argument ('--port', type = int, help = 'port number, from 1 to 65536; 0 by default; if 0 then assigned by os', default = 0)
parser.add_argument ('--password', help = 'password', default = '')
args = parser.parse_args ()

cfg_path = os.path.realpath (args.home)
cfg_port = args.port
cfg_password = args.password

# main class
class LabSyncServer :

	def __init__ (self) :
		global cfg_port, cfg_path

		self.api_master = api_master = API_master ()

		self.prev_fe, api_master.do_GET = api_master.do_GET, self.fe_GET

		self.server_14 = server_14 = Server14 (port = cfg_port, master = api_master)

		api_master.listen ('/stop', lambda a, b : server_14.stop () or "stopping")

		api_master.listen ('/sync/remove', self.on_remove)
		api_master.listen ('/sync/write', self.on_write)
		api_master.listen ('/sync/rename', self.on_rename)
		api_master.listen ('/sync/mkdir', self.on_mkdir)
		api_master.listen ('/sync/dump', self.on_dump)

		assigned_port = server_14.httpd.socket.getsockname ()[1]
		print ('started on port %d' % assigned_port)

		self.server_14.loop ()

	def fe_GET (self, handler) :
		global cfg_path
		try :
			with open (cfg_path + os.sep + handler.path, 'rb') as f :
				handler.respond_data (f.read ())
		except :
			self.prev_fe (handler)

	def on_remove (self, path, args) :
		if not self.check_password (args) :
			return 'access denied'
		self.remove (self.full_path (args['path']))

	def on_mkdir (self, path, args) :
		if not self.check_password (args) :
			return 'access denied'
		self.mkdir (self.full_path (args['path']))

	def fix_slashes (self, path) :
		return path if os.sep == '/' else path.replace ('\\', '/')

	def mkdir (self, path) :
		print ('mkdir %s' % self.hide (path))

		self.makedirs (path)
		try :
			os.mkdir (path)
		except :
			pass

	def on_write (self, path, args) :
		if not self.check_password (args) :
			return 'access denied'
		self.write (self.full_path (args['path']), args['__file'])

	def on_dump (self, path, args) :
		global cfg_path
		if not self.check_password (args) :
			return 'access denied'
		print ('dump')
		return self.dump (self.full_path ('.'))

	def on_rename (self, path, args) :
		if not self.check_password (args) :
			return 'access denied'
		path, path_2 = map (lambda k : self.full_path (args[k]), ('path', 'path_2'))
		if os.path.split (path)[0] != os.path.split (path_2)[0] :
			return 'bad args'
		self.rename (path, path_2)

	def check_password (self, args) :
		global cfg_password
		return 'password' in args and cfg_password == args['password']

	def makedirs (self, path) :
		try :
			os.makedirs (os.path.split (os.path.normpath (path))[0])
		except :
			pass

	def write (self, path, data) :
		print ('write (%d) %s' % (len (data), self.hide (path)))

		self.makedirs (path)
		with open (path, 'wb') as f : f.write (data)

	def rename (self, path, path_2) :
		print ('rename %s -> %s' % (self.hide (path), self.hide (path_2)))

		os.rename (path, path_2)

	def full_path (self, path) :
		global cfg_path

		return os.path.expanduser (cfg_path + os.sep + path)

	def remove (self, path) :
		print ('remove %s' % self.hide (path))

		if os.path.isfile (path) :
			os.remove (path)
		else :
			try :
				os.rmdir (path)
			except :
				shutil.rmtree (path)
				pass

	def hide (self, path) :
		return path
		# buggy
		##return '~' + path[len (os.path.expanduser ('~')):]

	def dump (self, path) :

		path = os.path.normpath (path)
		k = len (path) + 1

		def g (d, p, ch = False) :
			if ch :
				with open (p, 'rb') as f :
					h = hashlib.md5 (f.read ()).hexdigest ()
			else :
				h = ''
			d[self.fix_slashes (p[k:])] = [h, int (os.stat (p).st_mtime)]

		ad, af = {}, {}

		for t in os.walk (path) :
			cp, sd, sf = t
			if cp != path :
				g (ad, cp)
			for cf in sf :
				fn = cp + os.sep + cf
				with open (fn, 'rb') as f :
					g (af, fn, True)

		return [ad, af]

# set up
lab_sync_server = LabSyncServer ()
