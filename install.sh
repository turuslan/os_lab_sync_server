#!/bin/sh

if [ $# -ne 1 ];
	then echo "usage: install.sh PATH"
	exit
fi

wget https://bitbucket.org/turuslan/os_lab_sync_server/get/master.tar.gz -O os_lab_sync_server.tar.gz
mkdir $1
tar -xvzf os_lab_sync_server.tar.gz -C $1
mv $1/turuslan-os_lab_sync_server-????????????/* $1/
rmdir $1/turuslan-os_lab_sync_server-????????????
rm os_lab_sync_server.tar.gz

echo installation successful
